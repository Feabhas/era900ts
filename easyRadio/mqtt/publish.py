import paho.mqtt.client as paho
import time
import random
import argparse 


def on_publish(client, userdata, mid):
    print("mid: "+str(mid))


def main():

	parser = argparse.ArgumentParser(description='A simple program to publish to an MQTT feed') 
	parser.add_argument('-i','--ip', help='IP Address of MQTT Broker', required=True, nargs=1) 
	parser.add_argument('-t','--topic', help='Topic', required=True, nargs=1) 

	args = parser.parse_args() 

	ip_addr = args.ip[0]
	topic = args.topic[0]

	client = paho.Client()
	client.on_publish = on_publish
	client.connect(ip_addr, 1883)
	client.loop_start()
	 
	while True:
	    temperature = random.uniform(0, 100)
	    (rc, mid) = client.publish(topic, str(temperature), qos=0)
	    time.sleep(10)


if __name__ == '__main__':
    main()