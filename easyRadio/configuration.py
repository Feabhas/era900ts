#!/usr/bin/env python

# usage python echo.py --port /dev/cu.usbserial-A601YMNX --baud 38400

import argparse 

import serial
from serial.tools import list_ports

from time import sleep

import logging
from easyradio import easyRadio

def main():

	parser = argparse.ArgumentParser(description='A simple program to echo <CR> terminated message from an EasyRadio link') 
	parser.add_argument('-p','--port', help='USB Serial Port name', required=True, nargs=1) 
	parser.add_argument('-b','--baud', help='required baud rate', type=int, required=True, nargs=1) 

	args = parser.parse_args() 

	port = args.port[0]
	baud = args.baud[0]

	logging.basicConfig(level=logging.DEBUG)

	er = easyRadio(port, baud)

	print er.getTemperture()

	while True:
		cmd = raw_input("command: ")   # Python 2.x
		if 'exit' in cmd:
	 		exit()
		er.write(cmd)
		sleep(0.5)
		rcv_msg = er.read()
		logging.info("Message recvd: %s", rcv_msg)
	 	


if __name__ == '__main__':
    main()