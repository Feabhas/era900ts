#!/usr/bin/env python

# usage python echo.py --port /dev/cu.usbserial-A601YMNX --baud 38400

import argparse 

import serial
from serial.tools import list_ports

import logging
from easyradio import easyRadio

def main():

	parser = argparse.ArgumentParser(description='A simple program to listen for <CR> terminated message from an EasyRadio link') 
	parser.add_argument('-p','--port', help='USB Serial Port name', required=True, nargs=1) 
	parser.add_argument('-b','--baud', help='required baud rate', type=int, required=True, nargs=1) 

	args = parser.parse_args() 

	port = args.port[0]
	baud = args.baud[0]

	logging.basicConfig(level=logging.DEBUG)

	er = easyRadio(port, baud)

	while True:
		rcv_msg = er.read()
		logging.info("Message recvd: %s\n", rcv_msg)
	 	if 'exit' in rcv_msg:
	 		exit()


if __name__ == '__main__':
    main()