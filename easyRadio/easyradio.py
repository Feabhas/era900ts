#!/usr/bin/env python

import serial
from serial.tools import list_ports

from time import sleep

import logging

class easyRadio:

	ser = ""

	def __init__(self, thePort, theBaudrate):
		try:
			self.ser = serial.Serial(
				port=thePort, 
				baudrate=theBaudrate,
				parity = serial.PARITY_NONE,
				stopbits = serial.STOPBITS_ONE,
				timeout = None
				)
		except serial.SerialException:
			print("No know port, avaialble ports are")
			p = list_ports.comports()
			for i in p:
				print i.device
			exit()
		except serial.ValueError:
			logging.error("A parameter is out of range, e.g. baud rate, data bits.")
			exit()

		logging.info("Opened %s",self.ser.name)	
		logging.info("Device %s", self.getFirmwareRevision())

	def write(self, msg):
		logging.info("Sending message %s\n", msg)
		numWritten = self.ser.write(msg)
		if numWritten != len(msg):
			logging.error("message failed to send")
			exit()

	def read(self):
		while self.ser.in_waiting == 0:
			sleep(0.01)

		msg = ""
		while self.ser.in_waiting > 0:
			ch = self.ser.read(1)
			msg += ch
		return msg

	def readline(self):
		logging.info("readline: Waiting for message...")
		msg = self.ser.readline() #must be CR terminated
		return msg

	def getFirmwareRevision(self):
		self.write("ER_CMD#T3")
		sleep(0.01)
		response = self.read()
		if response == "ER_CMD#T3":
			self.write("ACK")
			sleep(0.01)
			response = self.read()
		return response

	def getTemperture(self):
		self.write("ER_CMD#T7")
		sleep(0.01)
		response = self.read()
		if response == "ER_CMD#T7":
			self.write("ACK")
			sleep(0.01)
			response = self.read()
		return response

def main():
	logging.basicConfig(level=logging.DEBUG)

	er = easyRadio('/dev/cu.usbserial-A601YMNX', 38400)

	send_msg = 'hello\n'.encode('utf-8')

	er.write(send_msg)

	rcv_msg = er.read()

	logging.info("Message recvd: %s", rcv_msg)


if __name__ == '__main__':
    main()