# Very Simple EasyRadio python class and echo program

This code was developed to test out the [LPRS](http://www.lprs.co.uk/index.html) EasyRadio [eRA Connect2 Pi](http://www.lprs.co.uk/wireless-modules/era-connect2-pi.html) modules. 

The testing was between a Raspberry Pi running RASPBIAN and a Mac running OSX. 

## Dependencies

* Python 2.7
* PySerial

## Test the radio link

On one target, e.g. Mac run the python echo program:
```
$ python echo.py --port /dev/cu.usbserial-A601YMNX --baud 38400
```

One the other you can use PySerial miniterm program
```
$ python -m serial.tools.miniterm /dev/ttyUSB0 38400
```
Now anything you type followed by a <CR> will be echo'ed

### Useful
If you are unsure of which port is being used simply type:
```
$ python -m serial.tools.list_ports
```